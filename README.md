# README #

## Plan ##

* Enter some GPS coordinates
* Generate one (or multiple) loops of GPS coordinates
    * These will have a given radius r 
    * We will select evenly spaced coordinates from around the loop
* Snap each coordinate point to the nearest road using google maps API
* Create a full loop of directions
    * Google maps API direction from point 1 -> 2, 2-> 3, .... n-1 -> n, n -> 1
* If we have multiple loops we need some way of determining the 'best' option
    * Train some ML model (or use an existing one) to find the closest fit to a circle
* Output our loop! 
    * Initially this could simply display it on maps
    * In future we could export as a downloadable GPS route map