from math import sin, cos, sqrt, atan2, radians


def get_distance_between_points(lat1, lon1, lat2, lon2):
    # approximate radius of earth in km
    R = 6373.0

    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    return distance


class Looper:
    def __init__(self, lat, lon, target_distance=5):
        self.lat = lat
        self.lon = lon
        self.target_distance = target_distance
        self.delta_lat = [0.00005, 0.00005, 0]
        self.delta_lon = [0, 0.00005, 0.00005]

    def update_coords(self, new_lat, new_lon, dlat, dlon):
        limit_reached = False
        while not limit_reached:
            new_lat += dlat
            new_lon += dlon
            distance = get_distance_between_points(lat1=self.lat, lon1=self.lon, lat2=new_lat, lon2=new_lon)
            if distance > self.target_distance:
                return [new_lat, new_lon]

    def gen_extra_loop_points(self, north=True, east=True):
        if not north:
            delta_lat = [-x for x in self.delta_lat]
        else:
            delta_lat = self.delta_lat
        if not east:
            delta_lon = [-x for x in self.delta_lon]
        else:
            delta_lon = self.delta_lon

        coords = [[self.lat, self.lon]]
        for dlat, dlon in zip(delta_lat, delta_lon):
            new_lat = self.lat + dlat
            new_lon = self.lon + dlon

            new_coords = self.update_coords(new_lat=new_lat, new_lon=new_lon, dlat=dlat, dlon=dlon)
            coords = coords + [new_coords]

        return coords


if __name__ == '__main__':
    starting_lattitude = 55.926004
    starting_longitude = -3.202369

    looper = Looper(lat=starting_lattitude, lon=starting_longitude)

    # north east
    print(looper.gen_extra_loop_points(north=True, east=True))

    # north west
    print(looper.gen_extra_loop_points(north=True, east=False))

    # south east
    print(looper.gen_extra_loop_points(north=-False, east=True))

    # south west
    print(looper.gen_extra_loop_points(north=-False, east=False))
